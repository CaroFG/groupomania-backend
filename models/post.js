'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Post extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
      this.belongsTo(models.User, { as: 'postAuthor', foreignKey: { name: 'postAuthorId', allowNull: false } })
      this.hasMany(models.Comment, { foreignKey: 'postId' })
    }
  };
  Post.init({
    content: {
      type: DataTypes.TEXT,
      allowNull: false,
      notEmpty: true
    },
    isCommentable: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    imageUrl: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'Post'
  })
  return Post
}
