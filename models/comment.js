'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Comment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
      this.belongsTo(models.Post, { as: 'Post', foreignKey: { name: 'postId', allowNull: false } })
      this.belongsTo(models.User, { as: 'CommentAuthor', foreignKey: { name: 'commentAuthorId', allowNull: false } })
      this.belongsTo(models.Comment, { as: 'ParentComment', foreignKey: 'parentCommentId' })
    }
  };
  Comment.init({
    content: {
      type: DataTypes.TEXT,
      allowNull: false,
      notEmpty: true
    },
    readByPostAuthor: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    readByParentCommentAuthor: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {
    sequelize,
    modelName: 'Comment'
  })
  return Comment
}
