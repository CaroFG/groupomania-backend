const { app } = require('./app')
const { errorHandler } = require('./helpers/errorHandler')
const { normalizePort } = require('./helpers/normalizePort')
const { sequelize } = require('./models/index')

const port = normalizePort(process.env.PORT || '3000')
app.set('port', port)

const server = app.listen(port)

const models = require('./models')

try {
  server.on('listening', () => {
    server.on('error', (e) => errorHandler(e, server, port))
    const address = server.address()
    const bind = typeof address === 'string' ? 'pipe ' + address : 'port ' + port
    console.log('Listening on ' + bind)
    sequelize.authenticate().then(() => {
      console.log('Connection to DB succeeded')
      models.sequelize.sync()
    })
  })
} catch (error) {
  console.error(error)
}
