const { errorHandler } = require('./errorHandler')

let myError, consoleErrorMock, mockExit
const server = { address: jest.fn() }
const port = 3000
beforeAll(function () {
  mockExit = jest.spyOn(process, 'exit').mockImplementation((number) => number)
  consoleErrorMock = jest.spyOn(console, 'error').mockImplementation(() => {})
})

describe('errorHandler', () => {
  test('should be a function', () => {
    expect(errorHandler).toBeInstanceOf(Function)
  })
  test('should throw the error passed as param if it\'s syscall is not "listen"', () => {
    myError = { syscall: 'link' }
    expect(() => { errorHandler(myError, server, port) }).toThrow(new Error(myError))
  })
  test('should exit process and print "requires elevated privileges" if error code is "EACCES"', () => {
    server.address.mockReturnValue(3000)
    myError = { syscall: 'listen', code: 'EACCES' }
    errorHandler(myError, server, port)
    expect(mockExit).toHaveBeenCalledWith(1)
    expect(consoleErrorMock).toHaveBeenCalledWith('port: 3000 requires elevated privileges.')
  })
  test('should exit process and print "is already in use" if error code is "EADDRINUSE"', () => {
    server.address.mockReturnValue('3000')
    myError = { syscall: 'listen', code: 'EADDRINUSE' }
    errorHandler(myError, server, port)
    expect(mockExit).toHaveBeenCalledWith(1)
    expect(consoleErrorMock).toHaveBeenCalledWith('pipe 3000 is already in use.')
  })
  test('should throw error if cases don\'t match value', () => {
    myError = { syscall: 'listen', code: 'ERR_HTTP_INVALID_HEADER_VALUE' }
    expect(() => { errorHandler(myError, server, port) }).toThrow(new Error(myError))
  })
})
