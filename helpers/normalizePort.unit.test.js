const { normalizePort } = require('./normalizePort')

describe('normalizePort', () => {
  let myNumber
  beforeEach(() => {
    myNumber = 3000
  })
  test('should be a function', () => {
    expect(normalizePort).toBeInstanceOf(Function)
  })
  test('should call the ParseInt function with the parameter value', () => {
    const parseIntMock = jest.spyOn(Number, 'parseInt')
    normalizePort(myNumber)
    expect(parseIntMock).toBeCalledWith(myNumber, 10)
  })
  test('should return value parameter if port is not a number', () => {
    jest.spyOn(Number, 'parseInt').mockImplementation(() => { return NaN })
    expect(normalizePort(myNumber)).toBe(myNumber)
    expect(normalizePort(myNumber)).not.toBe(NaN)
  })
  test('should return port if port is greater than or equal to 0', () => {
    jest.spyOn(Number, 'parseInt').mockImplementation(() => { return 8080 })
    expect(normalizePort(myNumber)).toBe(8080)
  })
  test('should return false if port is less than 0', () => {
    jest.spyOn(Number, 'parseInt').mockImplementation(() => { return -8080 })
    expect(normalizePort(myNumber)).toBe(false)
  })
})
