const errorHandler = (error, server, port) => {
  if (error.syscall !== 'listen') {
    throw error
  }
  const address = server.address()
  const bind = typeof address === 'string' ? 'pipe ' + address : 'port: ' + port
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges.')
      return process.exit(1)
    case 'EADDRINUSE':
      console.error(bind + ' is already in use.')
      return process.exit(1)
    default:
      throw error
  }
}

module.exports = { errorHandler }
