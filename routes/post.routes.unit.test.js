const postController = require('../controllers/post.controller')
const httpMocks = require('node-mocks-http')
const postRoutes = require('./post.routes')
const auth = require('../middleware/auth')
const multer = require('../middleware/multer.config')
jest.mock('../controllers/post.controller')
jest.mock('../middleware/auth')
jest.mock('../middleware/multer.config.js')

let req, res, next

beforeEach(() => {
  res = httpMocks.createResponse()
  next = jest.fn()
  auth.mockImplementation((req, res, next) => { return next() })
  multer.mockImplementation((req, res, next) => { return next() })
})
describe('post', () => {
  test('should call auth and multer', () => {
    req = httpMocks.createRequest({
      method: 'POST',
      url: '/'
    })
    postRoutes(req, res, next)
    expect(auth).toBeCalled()
    expect(multer).toBeCalled()
  })
  test('should call createPost if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'POST',
      url: '/'
    })
    postRoutes(req, res, next)
    expect(postController.createPost).toBeCalled()
  })
})

describe('get all', () => {
  test('should call auth', () => {
    req = httpMocks.createRequest({
      method: 'GET',
      url: '/'
    })
    postRoutes(req, res, next)
    expect(auth).toBeCalled()
  })
  test('should call postController.getAllPosts if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'GET',
      url: '/'
    })
    postRoutes(req, res, next)
    expect(postController.getAllPosts).toBeCalled()
  })
})

describe('get', () => {
  test('should call auth', () => {
    req = httpMocks.createRequest({
      method: 'GET',
      url: '/:id'
    })
    postRoutes(req, res, next)
    expect(auth).toBeCalled()
  })
  test('should call postController.getPost if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'GET',
      url: '/:id'
    })
    postRoutes(req, res, next)
    expect(postController.getPost).toBeCalled()
  })
})

describe('put', () => {
  test('should call auth and multer', () => {
    req = httpMocks.createRequest({
      method: 'PUT',
      url: '/:id'
    })
    postRoutes(req, res, next)
    expect(auth).toBeCalled()
    expect(multer).toBeCalled()
  })
  test('should call postController.updatePost if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'PUT',
      url: '/:id'
    })
    postRoutes(req, res, next)
    expect(postController.updatePost).toBeCalled()
  })
})
describe('delete', () => {
  test('should call auth', () => {
    req = httpMocks.createRequest({
      method: 'DELETE',
      url: '/:id'
    })
    postRoutes(req, res, next)
    expect(auth).toBeCalled()
  })
  test('should call postController.deletePost if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'DELETE',
      url: '/:id'
    })
    postRoutes(req, res, next)
    expect(postController.deletePost).toBeCalled()
  })
})
