const commentController = require('../controllers/comment.controller')
const httpMocks = require('node-mocks-http')
const commentRoutes = require('./comment.routes')
const auth = require('../middleware/auth')
jest.mock('../controllers/comment.controller')
jest.mock('../middleware/auth')

let req, res, next

beforeEach(() => {
  res = httpMocks.createResponse()
  next = jest.fn()
  auth.mockImplementation((req, res, next) => { return next() })
})
describe('post', () => {
  test('should call auth', () => {
    req = httpMocks.createRequest({
      method: 'POST',
      url: '/:id/comments/'
    })
    commentRoutes(req, res, next)
    expect(auth).toBeCalled()
  })
  test('should call createComment if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'POST',
      url: '/:id/comments/'
    })
    commentRoutes(req, res, next)
    expect(commentController.createComment).toBeCalled()
  })
})

describe('get all', () => {
  test('should call auth', () => {
    req = httpMocks.createRequest({
      method: 'GET',
      url: '/:id/comments/'
    })
    commentRoutes(req, res, next)
    expect(auth).toBeCalled()
  })
  test('should call commentController.getAllComments if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'GET',
      url: '/:id/comments/'
    })
    commentRoutes(req, res, next)
    expect(commentController.getAllComments).toBeCalled()
  })
})

describe('get', () => {
  test('should call auth', () => {
    req = httpMocks.createRequest({
      method: 'GET',
      url: '/:id/comments/:comment_id'
    })
    commentRoutes(req, res, next)
    expect(auth).toBeCalled()
  })
  test('should call commentController.getComment if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'GET',
      url: '/:id/comments/:comment_id'
    })
    commentRoutes(req, res, next)
    expect(commentController.getComment).toBeCalled()
  })
})

describe('put', () => {
  test('should call auth', () => {
    req = httpMocks.createRequest({
      method: 'PUT',
      url: '/:id/comments/:comment_id'
    })
    commentRoutes(req, res, next)
    expect(auth).toBeCalled()
  })
  test('should call commentController.updateComment if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'PUT',
      url: '/:id/comments/:comment_id'
    })
    commentRoutes(req, res, next)
    expect(commentController.updateComment).toBeCalled()
  })
})
describe('delete', () => {
  test('should call auth', () => {
    req = httpMocks.createRequest({
      method: 'DELETE',
      url: '/:id/comments/:comment_id'
    })
    commentRoutes(req, res, next)
    expect(auth).toBeCalled()
  })
  test('should call commentController.deleteComment if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'DELETE',
      url: '/:id/comments/:comment_id'
    })
    commentRoutes(req, res, next)
    expect(commentController.deleteComment).toBeCalled()
  })
})
