const express = require('express')
const router = express.Router()
const postController = require('../controllers/post.controller')
const auth = require('../middleware/auth')
const multer = require('../middleware/multer.config')

router.post('/', auth, multer, postController.createPost)
router.get('/:id', auth, postController.getPost)
router.get('/', auth, postController.getAllPosts)
router.put('/:id', auth, multer, postController.updatePost)
router.delete('/:id', auth, postController.deletePost)

module.exports = router
