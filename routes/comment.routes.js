const express = require('express')
const router = express.Router()
const commentController = require('../controllers/comment.controller')
const auth = require('../middleware/auth')

router.post('/:id/comments/', auth, commentController.createComment)
router.get('/:id/comments/:comment_id', auth, commentController.getComment)
router.get('/:id/comments/', auth, commentController.getAllComments)
router.put('/:id/comments/:comment_id', auth, commentController.updateComment)
router.delete('/:id/comments/:comment_id', auth, commentController.deleteComment)

module.exports = router
