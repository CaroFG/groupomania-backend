const userController = require('../controllers/user.controller')
const httpMocks = require('node-mocks-http')
const userRoutes = require('./user.routes')
const auth = require('../middleware/auth')
const multer = require('../middleware/multer.config')
jest.mock('../controllers/user.controller')
jest.mock('../middleware/auth')
jest.mock('../middleware/multer.config')

beforeEach(() => {
  auth.mockImplementation((req, res, next = jest.fn()) => { return next() })
})

test('should call userController.signup', () => {
  const request = httpMocks.createRequest({
    method: 'POST',
    url: '/signup'
  })
  userRoutes(request)
  expect(userController.signup).toBeCalled()
})

test('should call userController.login', () => {
  const request = httpMocks.createRequest({
    method: 'POST',
    url: '/login'
  })
  userRoutes(request)
  expect(userController.login).toBeCalled()
})

describe('LOGOUT', () => {
  test('should call auth', () => {
    const request = httpMocks.createRequest({
      method: 'POST',
      url: '/logout'
    })
    userRoutes(request)
    expect(auth).toBeCalled()
  })
  test('should call userController.logout', () => {
    const request = httpMocks.createRequest({
      method: 'POST',
      url: '/logout'
    })
    userRoutes(request)
    expect(userController.logout).toBeCalled()
  })
})

describe('PUT', () => {
  test('should call auth and multer', () => {
    const request = httpMocks.createRequest({
      method: 'PUT',
      url: '/:id'
    })
    userRoutes(request)
    expect(auth).toBeCalled()
    expect(multer).toBeCalled()
  })
  test('should call userController.updateUserInfo', () => {
    multer.mockImplementation((req, res, next) => { return next() })
    const request = httpMocks.createRequest({
      method: 'PUT',
      url: '/:id'
    })
    userRoutes(request)
    expect(userController.updateUserInfo).toBeCalled()
  })
})
describe('GET', () => {
  test('should call auth', () => {
    const request = httpMocks.createRequest({
      method: 'GET',
      url: '/'
    })
    userRoutes(request)
    expect(auth).toBeCalled()
  })
  test('should call userController.getAllUsers', () => {
    const request = httpMocks.createRequest({
      method: 'GET',
      url: '/'
    })
    userRoutes(request)
    expect(userController.getAllUsers).toBeCalled()
  })
})
describe('GET BY ID', () => {
  test('should callauth', () => {
    const request = httpMocks.createRequest({
      method: 'GET',
      url: '/:id'
    })
    userRoutes(request)
    expect(auth).toBeCalled()
  })
  test('should call userController.getUser', () => {
    const request = httpMocks.createRequest({
      method: 'GET',
      url: '/:id'
    })
    userRoutes(request)
    expect(userController.getUser).toBeCalled()
  })
})
describe('DELETE', () => {
  test('should call auth', () => {
    const request = httpMocks.createRequest({
      method: 'DELETE',
      url: '/:id'
    })
    userRoutes(request)
    expect(auth).toBeCalled()
  })
  test('should call userController.deleteUser', () => {
    const request = httpMocks.createRequest({
      method: 'DELETE',
      url: '/:id'
    })
    userRoutes(request)
    expect(userController.deleteUser).toBeCalled()
  })
})
