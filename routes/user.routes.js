const express = require('express')
const router = express.Router()
const userController = require('../controllers/user.controller')
const auth = require('../middleware/auth')
const multer = require('../middleware/multer.config')

router.post('/signup', userController.signup)
router.post('/login', userController.login)
router.post('/logout', auth, userController.logout)
router.put('/:id', auth, multer, userController.updateUserInfo)
router.get('/', auth, userController.getAllUsers)
router.get('/:id', auth, userController.getUser)
router.delete('/:id', auth, userController.deleteUser)

module.exports = router
