const userController = require('./user.controller')
const models = require('../models')
const httpMocks = require('node-mocks-http')
const bcrypt = require('bcrypt')
const fs = require('fs')

jest.mock('../models')

let req, res, user, userModelMock, expectedMsg

beforeEach(() => {
  req = httpMocks.createRequest()
  res = httpMocks.createResponse()
  user = { email: 'test@example.com', password: '%testpassword01', firstName: 'John', lastName: 'Doe' }
})

describe('userController.signup', () => {
  test('should have a signup function', () => {
    expect(userController.signup).toBeInstanceOf(Function)
  })
  test('should return a 201 response code if mandatory fields are provided', async () => {
    req.body = user
    try {
      await userController.signup(req, res)
      expect(res.statusCode).toBe(201)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should return error if password is missing', async () => {
    req.body = {}
    try {
      await userController.signup(req, res)
      expect(res.statusCode).toBe(500)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should return error if field is missing', async () => {
    req.body.password = 'password'
    req.body.email = {}
    try {
      await userController.signup(req, res)
      expect(res.statusCode).toBe(400)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should return error if email already exists', async () => {
    req.body = user
    models.User.findOne.mockReturnValue(user)
    try {
      await userController.signup(req, res)
      expect(res.statusCode).toBe(409)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
})

describe('userController.login', () => {
  test('should have a login password', async () => {
    expect(userController.login).toBeInstanceOf(Function)
  })
  test('should return a 401 response status code if no user is found with the given email', async () => {
    req.body = user
    userModelMock = jest.spyOn(models.User, 'findOne')
    userModelMock.mockReturnValue(null)
    try {
      await userController.login(req, res)
      expect(res.statusCode).toBe(401)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should compare DB and request password and return 401 status code if they do not match', async () => {
    req.body = user
    expectedMsg = 'Email ou mot de passe incorrect'
    userModelMock = jest.spyOn(models.User, 'findOne')
    userModelMock.mockReturnValue(user)
    jest.spyOn(bcrypt, 'compare').mockReturnValue(false)
    try {
      await userController.login(req, res)
      expect(res.statusCode).toBe(401)
      expect(res._getJSONData().error).toEqual(expectedMsg)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should compare DB and request password and return 200 status code if they match', async () => {
    const returnedUser = { id: 2, email: 'test@example.com', password: 'hashedpassword' }
    userModelMock = jest.spyOn(models.User, 'findOne')
    userModelMock.mockReturnValue(returnedUser)
    jest.spyOn(bcrypt, 'compare').mockReturnValue(true)
    req.body = user
    req.session = {}
    try {
      await userController.login(req, res)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().user).toEqual(returnedUser)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should assign user.id to req.session.userId', async () => {
    const returnedUser = { id: 2, email: 'test@example.com', password: 'hashedpassword' }
    userModelMock = jest.spyOn(models.User, 'findOne')
    userModelMock.mockReturnValue(returnedUser)
    jest.spyOn(bcrypt, 'compare').mockReturnValue(true)
    req.body = user
    req.session = {}
    try {
      await userController.login(req, res)
      expect(req.session.userId).toBe(returnedUser.id)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors if bcrypt rejects', async () => {
    userModelMock = jest.spyOn(models.User, 'findOne')
    userModelMock.mockReturnValue(user)
    jest.spyOn(bcrypt, 'compare').mockRejectedValue()
    req.body = user
    try {
      await userController.login(req, res)
      expect(res.statusCode).toBe(500)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors if models.User.findOne rejects', async () => {
    userModelMock = jest.spyOn(models.User, 'findOne')
    userModelMock.mockRejectedValue()
    req.body = user
    try {
      await userController.login(req, res)
      expect(res.statusCode).toBe(500)
      expect(res._getJSONData().error).toEqual('Oups ! Une erreur est survenue, veuillez nous en excuser')
    } catch (error) {
      expect(true).toBe(false)
    }
  })
})

describe('userController.logout', () => {
  beforeEach(() => {
    req.session = { destroy: jest.fn() }
  })
  test('should have a logout function', () => {
    expect(userController.logout).toBeInstanceOf(Function)
  })
  test('should call session destroy function', async () => {
    // req.session = { destroy: jest.fn() }
    try {
      await userController.logout(req, res)
      expect(req.session.destroy).toHaveBeenCalled()
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    req.session.destroy.mockRejectedValue()
    try {
      await userController.logout(req, res)
      expect(res.statusCode).toBe(500)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
})

describe('userController.updateUserInfo', () => {
  beforeEach(() => {
    req.session = {}
    beforeEach(() => { models.User.findByPk.mockReturnValue({ profilePic: '/images/profile.png' }) })
  })
  test('should have an updateUserInfo function', () => {
    expect(userController.updateUserInfo).toBeInstanceOf(Function)
  })
  test('should call models.User.findByPk  and update methods', async () => {
    const userModelMockFind = jest.spyOn(models.User, 'findByPk')
    const userModelMockUpdate = jest.spyOn(models.User, 'update')
    try {
      await userController.updateUserInfo(req, res)
      expect(userModelMockFind).toHaveBeenCalled()
      expect(userModelMockUpdate).toHaveBeenCalled()
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should return response with status 200 and message without file', async () => {
    const userInfo = { firstName: 'Paul', last_name: 'Monet' }
    req.params.id = '33'
    req.body = userInfo
    try {
      await userController.updateUserInfo(req, res)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().message).toEqual('User updated successfully')
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should return response with status 200 and message with a file', async () => {
    const userInfo = { firstName: 'Paul', last_name: 'Monet' }
    req.protocol = 'http'
    req.file = { filename: 'myImage' }
    req.params.id = '33'
    req.body = JSON.stringify(userInfo)
    try {
      await userController.updateUserInfo(req, res)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().message).toEqual('User updated successfully')
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should call findByPk and remove old file when updated with new file', async () => {
    const userInfo = { firstName: 'Paul', last_name: 'Monet' }
    req.body = JSON.stringify(userInfo)
    req.protocol = 'http'
    req.file = { filename: 'myImage' }
    req.params = { id: 51 }
    const fsMock = jest.spyOn(fs, 'unlink')
    try {
      await userController.updateUserInfo(req, res)
      expect(models.User.findByPk).toBeCalled()
      expect(fsMock).toBeCalledWith('images/profile.png', expect.any(Function))
      fsMock.mockRestore()
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    models.User.update.mockRejectedValue()
    try {
      await userController.updateUserInfo(req, res)
      expect(res.statusCode).toBe(500)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
})

describe('userController.getAllUsers', () => {
  test('should have a getAllUsers function', async () => {
    expect(userController.getAllUsers).toBeInstanceOf(Function)
  })
  test('should call models.User.findAll method', async () => {
    try {
      await userController.getAllUsers(req, res)
      expect(models.User.findAll).toHaveBeenCalled()
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should send a 200 status code and all users', async () => {
    const allUsers = [user, user]
    models.User.findAll.mockReturnValue(allUsers)
    try {
      await userController.getAllUsers(req, res)
      expect(models.User.findAll).toHaveBeenCalled()
      expect(res._getJSONData()).toEqual(allUsers)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    models.User.findAll.mockRejectedValue()
    try {
      await userController.getAllUsers(req, res)
      expect(res.statusCode).toBe(500)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})

describe('userController.getUser', () => {
  test('should have a getUser function', () => {
    expect(userController.getUser).toBeInstanceOf(Function)
  })
  test('should call models.User.findByPk', async () => {
    try {
      await userController.getUser(req, res)
      expect(models.User.findByPk).toHaveBeenCalled()
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should return response with status 200 and the user', async () => {
    req.params.id = '25'
    models.User.findByPk.mockReturnValue(user)
    try {
      await userController.getUser(req, res)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData()).toEqual(user)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should return 404 status code if user === null', async () => {
    req.params.id = '25'
    models.User.findByPk.mockReturnValue(null)
    try {
      await userController.getUser(req, res)
      expect(res.statusCode).toBe(404)
      expect(res._getJSONData().error).toEqual('Utilisateur non trouvé')
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    req.params.id = '25'
    models.User.findByPk.mockRejectedValue()
    try {
      await userController.getUser(req, res)
      expect(res.statusCode).toBe(404)
      expect(res._getJSONData().error).toEqual('Utilisateur non trouvé')
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})

describe('userController.deleteUser', () => {
  test('should have a deleteUser function', () => {
    expect(userController.deleteUser).toBeInstanceOf(Function)
  })
  test('should call models.User.findByPk method', async () => {
    jest.spyOn(fs, 'unlink')
    try {
      await userController.deleteUser(req, res)
      expect(models.User.findByPk).toHaveBeenCalled()
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should call fs.unlink with filename and anonymous function if user has image', async () => {
    req.params.id = '25'
    models.User.findByPk.mockReturnValue({ profilePic: '/images/profile.png' })
    const fsMock = jest.spyOn(fs, 'unlink')
    try {
      await userController.deleteUser(req, res)
      expect(res.statusCode).toBe(200)
      expect(fsMock).toBeCalledWith('images/profile.png', expect.any(Function))
      fsMock.mockRestore()
      expect(models.User.destroy).toBeCalled()
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should call models.User.destroy if user has no image and res status 200', async () => {
    req.params.id = '25'
    models.User.findByPk.mockReturnValue({ firstName: 'Marianne' })
    try {
      await userController.deleteUser(req, res)
      expect(models.User.destroy).toBeCalled()
      expect(res.statusCode).toBe(200)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    models.User.findByPk.mockRejectedValue()
    try {
      await userController.deleteUser(req, res)
      expect(res.statusCode).toBe(400)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})
