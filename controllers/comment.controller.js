const models = require('../models')

exports.createComment = async (req, res) => {
  try {
    const postId = req.params.id
    await models.Comment.create({ ...req.body, postId: postId })
    return res.status(201).json({ message: 'Commentaire enregistré avec succès' })
  } catch (error) {
    return res.status(400).json({ error: error })
  }
}

exports.getComment = async (req, res) => {
  try {
    const commentId = req.params.comment_id
    const comment = await models.Comment.findByPk(commentId)
    if (comment === null) {
      res.status(404).json({ error: 'Commentaire non trouvé' })
    } else {
      res.status(200).json(comment)
    }
  } catch (error) {
    res.status(404).json({ error: 'Commentaire non trouvé' })
  }
}

exports.getAllComments = async (req, res) => {
  try {
    const postId = req.params.id
    const comments = await models.Comment.findAll({ where: { postId } })
    res.status(200).json(comments)
  } catch (error) {
    res.status(500).json({ error: 'Oups ! Un problème est survenu, veuillez nous en excuser' })
  }
}

exports.updateComment = async (req, res) => {
  try {
    const commentId = req.params.comment_id
    await models.Comment.update(req.body, {
      where: { id: commentId }
    })
    res.status(200).json({ message: 'Comment updated successfully' })
  } catch (error) {
    res.status(500).json({ error: 'Oups ! Un problème est survenu, veuillez nous en excuser' })
  }
}

exports.deleteComment = async (req, res) => {
  try {
    const commentId = req.params.comment_id
    await models.Comment.destroy({
      where: { id: commentId }
    })
    res.status(200).json({ message: 'Comment deleted successfully' })
  } catch (error) {
    res.status(400).json({ error })
  }
}
