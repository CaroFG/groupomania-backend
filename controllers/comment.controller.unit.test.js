const commentController = require('./comment.controller')
const models = require('../models')
const httpMocks = require('node-mocks-http')

const comment = { content: 'Some text' }

jest.mock('../models')

let req, res

beforeEach(() => {
  req = httpMocks.createRequest()
  res = httpMocks.createResponse()
})

describe('commentController.createComment', () => {
  test('should have a createComment function', () => {
    expect(commentController.createComment).toBeInstanceOf(Function)
  })
  test('should return a 201 response code if comment info is provided', async () => {
    req.params = { id: '46' }
    req.body = { comment: comment, commentAuthorId: '35' }
    try {
      await commentController.createComment(req, res)
      expect(res.statusCode).toBe(201)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should return error if comment property is missing', async () => {
    req.body.comment = {}
    const commentModelMock = jest.spyOn(models.Comment, 'create')
    commentModelMock.mockRejectedValue()
    try {
      await commentController.createComment(req, res)
      expect(res.statusCode).toBe(400)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})

describe('commentController.getAllComments', () => {
  beforeEach(() => {
    req.params = { id: '46' }
  })
  test('should have a getAllComments function', async () => {
    expect(commentController.getAllComments).toBeInstanceOf(Function)
  })
  test('should call models.Comment.findAll method', async () => {
    try {
      await commentController.getAllComments(req, res)
      expect(models.Comment.findAll).toHaveBeenCalled()
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should send a 200 status code and all comments', async () => {
    const allComments = [comment, comment]
    models.Comment.findAll.mockReturnValue(allComments)
    try {
      await commentController.getAllComments(req, res)
      expect(models.Comment.findAll).toHaveBeenCalled()
      expect(res._getJSONData()).toEqual(allComments)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    models.Comment.findAll.mockRejectedValue()
    try {
      await commentController.getAllComments(req, res)
      expect(res.statusCode).toBe(500)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})

describe('commentController.getComment', () => {
  test('should have a getComment function', () => {
    expect(commentController.getComment).toBeInstanceOf(Function)
  })
  test('should call models.Comment.findByPk', async () => {
    try {
      await commentController.getComment(req, res)
      expect(models.Comment.findByPk).toHaveBeenCalled()
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should return response with status 200 and the comment', async () => {
    req.params.id = '25'
    models.Comment.findByPk.mockReturnValue(comment)
    try {
      await commentController.getComment(req, res)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData()).toEqual(comment)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should return 404 status code if comment === null', async () => {
    req.params.id = '25'
    models.Comment.findByPk.mockReturnValue(null)
    try {
      await commentController.getComment(req, res)
      expect(res.statusCode).toBe(404)
      expect(res._getJSONData().error).toEqual('Commentaire non trouvé')
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    req.params.id = '25'
    models.Comment.findByPk.mockRejectedValue()
    try {
      await commentController.getComment(req, res)
      expect(res.statusCode).toBe(404)
      expect(res._getJSONData().error).toEqual('Commentaire non trouvé')
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})

describe('commentController.updateComment', () => {
  test('should have an updateComment function', () => {
    expect(commentController.updateComment).toBeInstanceOf(Function)
  })
  test('should call models.Comment.update method', async () => {
    try {
      await commentController.updateComment(req, res)
      expect(models.Comment.update).toHaveBeenCalled()
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should return response with status 200 and message', async () => {
    req.params.comment_id = '21'
    req.body.comment = comment
    try {
      await commentController.updateComment(req, res)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().message).toBe('Comment updated successfully')
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    req.params.comment_id = '21'
    req.body.comment = comment
    models.Comment.update.mockRejectedValue()
    try {
      await commentController.updateComment(req, res)
      expect(res.statusCode).toBe(500)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})

describe('commentController.deleteComment', () => {
  test('should have a deleteComment function', () => {
    expect(commentController.deleteComment).toBeInstanceOf(Function)
  })
  test('should call models.Comment.destroy method', async () => {
    try {
      await commentController.deleteComment(req, res)
      expect(models.Comment.destroy).toHaveBeenCalled()
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should return response with 200 status code and message', async () => {
    req.params.comment_id = '25'
    try {
      await commentController.deleteComment(req, res)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().message).toBe('Comment deleted successfully')
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    models.Comment.destroy.mockRejectedValue()
    try {
      await commentController.deleteComment(req, res)
      expect(res.statusCode).toBe(400)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})
