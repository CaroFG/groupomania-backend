const models = require('../models')
const fs = require('fs')

exports.createPost = async (req, res) => {
  try {
    const postObjet = req.file
      ? {
        ...req.body,
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
      } : {
        ...req.body
      }
    await models.Post.create(postObjet)
    return res.status(201).json({ message: 'Post saved successfully' })
  } catch (error) {
    return res.status(400).json({ error: error })
  }
}

exports.getPost = async (req, res) => {
  try {
    const postId = req.params.id || req.body.id
    const post = await models.Post.findByPk(postId, { include: models.Comments })
    if (post === null) {
      res.status(404).json({ message: 'Post not found' })
    } else {
      res.status(200).json(post)
    }
  } catch (error) {
    res.status(404).json({ message: 'Post not found' })
  }
}

exports.getAllPosts = async (req, res) => {
  try {
    const posts = await await models.Post.findAll({ order: [['createdAt', 'DESC']], include: [{ model: models.User, as: 'postAuthor' }, { model: models.Comment, order: [['createdAt', 'ASC']], include: { model: models.User, as: 'CommentAuthor' } }] })
    res.status(200).json(posts)
  } catch (error) {
    res.status(500).json({ error: 'Oups ! Un problème est survenu, veuillez nous en excuser' })
  }
}

exports.updatePost = async (req, res) => {
  try {
    const postId = req.params.id
    const post = await models.Post.findByPk(postId)
    let postObjet
    if (post.imageUrl && req.file) {
      const filename = post.imageUrl.split('/images/')[1]
      fs.unlink(`images/${filename}`, async () => {
        try {
          postObjet = {
            ...req.body,
            imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
          }
          await models.Post.update(postObjet, {
            where: { id: postId }
          })
          res.status(200).json({ message: 'Post updated successfully' })
        } catch (error) {
          res.status(500).json({ error: 'Oups ! Un problème est survenu, veuillez nous en excuser' })
        }
      })
    }
    postObjet = req.file
      ? {
        ...req.body,
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
      } : {
        ...req.body
      }
    await models.Post.update(postObjet, {
      where: { id: postId }
    })
    res.status(200).json({ message: 'Post updated successfully' })
  } catch (error) {
    res.status(500).json({ error: 'Oups ! Un problème est survenu, veuillez nous en excuser' })
  }
}

exports.deletePost = async (req, res) => {
  try {
    const postId = req.params.id || req.body.id
    const post = await models.Post.findByPk(postId)
    if (post.imageUrl) {
      const filename = post.imageUrl.split('/images/')[1]
      fs.unlink(`images/${filename}`, async () => {
        try {
          await models.Post.destroy({
            where: { id: postId }
          })
          res.status(200).json({ message: 'Post deleted successfully' })
        } catch (error) {
          res.status(400).json({ error })
        }
      })
    }
    await models.Post.destroy({
      where: { id: postId }
    })
    res.status(200).json({ message: 'Post deleted successfully' })
  } catch (error) {
    res.status(400).json({ error })
  }
}
