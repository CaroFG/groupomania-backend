const postController = require('./post.controller')
const models = require('../models')
const httpMocks = require('node-mocks-http')
const fs = require('fs')

const post = { content: 'Some text' }

jest.mock('../models')

let req, res

beforeEach(() => {
  req = httpMocks.createRequest()
  res = httpMocks.createResponse()
  jest.spyOn(fs, 'unlink')
})

describe('postController.createPost', () => {
  test('should have a createPost function', () => {
    expect(postController.createPost).toBeInstanceOf(Function)
  })
  test('should return a 201 response code if post info is provided', async () => {
    req.body = post
    try {
      await postController.createPost(req, res)
      expect(res.statusCode).toBe(201)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should return error if post property is missing', async () => {
    req.body = {}
    const PostModelMock = jest.spyOn(models.Post, 'create')
    PostModelMock.mockRejectedValue()
    try {
      await postController.createPost(req, res)
      expect(res.statusCode).toBe(400)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})

describe('postController.getAllPosts', () => {
  test('should have a getAllPosts function', async () => {
    expect(postController.getAllPosts).toBeInstanceOf(Function)
  })
  test('should call models.Post.findAll method', async () => {
    try {
      await postController.getAllPosts(req, res)
      expect(models.Post.findAll).toHaveBeenCalled()
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should send a 200 status code and all posts', async () => {
    const allPosts = [post, post]
    models.Post.findAll.mockReturnValue(allPosts)
    try {
      await postController.getAllPosts(req, res)
      expect(models.Post.findAll).toHaveBeenCalled()
      expect(res._getJSONData()).toEqual(allPosts)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    models.Post.findAll.mockRejectedValue()
    try {
      await postController.getAllPosts(req, res)
      expect(res.statusCode).toBe(500)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})

describe('postController.getPost', () => {
  test('should have a getPost function', () => {
    expect(postController.getPost).toBeInstanceOf(Function)
  })
  test('should call models.Post.findByPk', async () => {
    try {
      await postController.getPost(req, res)
      expect(models.Post.findByPk).toHaveBeenCalled()
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should return response with status 200 and the post', async () => {
    req.params.id = '25'
    models.Post.findByPk.mockReturnValue(post)
    try {
      await postController.getPost(req, res)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData()).toEqual(post)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should return 404 status code if post === null', async () => {
    req.params.id = '25'
    models.Post.findByPk.mockReturnValue(null)
    try {
      await postController.getPost(req, res)
      expect(res.statusCode).toBe(404)
      expect(res._getJSONData().message).toEqual('Post not found')
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    req.params.id = '25'
    models.Post.findByPk.mockRejectedValue()
    try {
      await postController.getPost(req, res)
      expect(res.statusCode).toBe(404)
      expect(res._getJSONData().message).toEqual('Post not found')
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})

describe('postController.updatePost', () => {
  beforeEach(() => { req.params.id = '33' })
  test('should have an updatePost function', () => {
    expect(postController.updatePost).toBeInstanceOf(Function)
  })
  test('should call models.Post.findeByPk method', async () => {
    try {
      await postController.updatePost(req, res)
      expect(models.Post.findByPk).toHaveBeenCalled()
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should return response with status 200 and message without file', async () => {
    models.Post.findByPk.mockReturnValue(post)
    req.body = post
    req.file = false
    try {
      await postController.updatePost(req, res)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().message).toBe('Post updated successfully')
    } catch (err) {
      console.log(err, '***')
      expect(true).toBe(false)
    }
  })
  test('should return response with status 200 and message with a file', async () => {
    models.Post.findByPk.mockReturnValue({ conten: 'content', imageUrl: 'images/myOldImage.jpg' })
    req.protocol = 'http'
    req.file = { filename: 'myImage' }
    req.params.id = '33'
    req.body = JSON.stringify(post)
    try {
      await postController.updatePost(req, res)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().message).toEqual('Post updated successfully')
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should call findByPk and remove old file when updated with new file', async () => {
    models.Post.findByPk.mockReturnValue({ conten: 'content', imageUrl: '/images/myOldImage.jpg' })
    req.body = JSON.stringify(post)
    req.protocol = 'http'
    req.file = { filename: 'myImage' }
    const fsMock = jest.spyOn(fs, 'unlink')
    try {
      await postController.updatePost(req, res)
      expect(models.Post.findByPk).toBeCalled()
      expect(fsMock).toBeCalledWith('images/myOldImage.jpg', expect.any(Function))
      fsMock.mockRestore()
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    req.params.id = '21'
    req.body.post = post
    models.Post.update.mockRejectedValue()
    try {
      await postController.updatePost(req, res)
      expect(res.statusCode).toBe(500)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})

describe('postController.deletePost', () => {
  test('should have a deletePost function', () => {
    expect(postController.deletePost).toBeInstanceOf(Function)
  })
  test('should call models.Post.findByPk method', async () => {
    jest.spyOn(fs, 'unlink')
    try {
      await postController.deletePost(req, res)
      expect(models.Post.findByPk).toHaveBeenCalled()
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should call fs.unlink if post has image and return 200 status code', async () => {
    req.params.id = '25'
    models.Post.findByPk.mockReturnValue({ imageUrl: '/images/picture.png' })
    const fsMock = jest.spyOn(fs, 'unlink')
    try {
      await postController.deletePost(req, res)
      expect(res.statusCode).toBe(200)
      expect(fsMock).toBeCalledWith('images/picture.png', expect.any(Function))
      fsMock.mockRestore()
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should call models.Post.destroy and 200 status code if post has no image', async () => {
    req.params.id = '25'
    models.Post.findByPk.mockReturnValue(post)
    try {
      await postController.deletePost(req, res)
      expect(res.statusCode).toBe(200)
      expect(models.Post.destroy).toBeCalled()
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    models.Post.findByPk.mockRejectedValue()
    try {
      await postController.deletePost(req, res)
      expect(res.statusCode).toBe(400)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})
