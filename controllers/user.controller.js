const models = require('../models')
const bcrypt = require('bcrypt')
const fs = require('fs')

exports.signup = async (req, res, next) => {
  const isFieldMissing = !req.body.email || !req.body.password || !req.body.firstName || !req.body.lastName
  const userExists = await models.User.findOne({ where: { email: req.body.email } })
  try {
    const hash = await bcrypt.hash(req.body.password, 10)
    try {
      if (isFieldMissing) {
        throw new Error('Tous les champs sont obligatoires')
      }
      if (userExists) {
        return res.status(409).json({ error: 'Un compte existe déjà pour cette adresse e-mail' })
      }
      await models.User.create({
        email: req.body.email,
        password: hash,
        firstName: req.body.firstName,
        lastName: req.body.lastName
      })
      res.status(201).json({ message: 'User created successfully' })
    } catch (error) {
      res.status(400).json({ error: 'Tous les champs sont obligatoires' })
    }
  } catch (error) {
    res.status(500).json({ error: 'Oups ! Une erreur est survenue, veuillez nous en excuser' })
  }
}

exports.login = async (req, res, next) => {
  try {
    const user = await models.User.findOne({ where: { email: req.body.email } })
    if (!user) {
      return res.status(401).json({ error: 'Email ou mot de passe incorrect' })
    }

    const isValid = await bcrypt.compare(req.body.password, user.password)
    if (!isValid) {
      return res.status(401).json({ error: 'Email ou mot de passe incorrect' })
    }
    const unreadComments = await models.Post.findAll({ where: { postAuthorId: user.id }, include: { model: models.Comment, where: { readByPostAuthor: false }, include: { model: models.User, as: 'CommentAuthor' } } })
    req.session.userId = user.id
    res.status(200).json({
      user,
      unreadComments
    })
  } catch (error) {
    res.status(500).json({ error: 'Oups ! Une erreur est survenue, veuillez nous en excuser' })
  }
}

exports.logout = async (req, res) => {
  try {
    await req.session.destroy()
    res.clearCookie(process.env.SESS_NAME)
  } catch (error) {
    res.status(500).json({ error: 'Oups ! Une erreur est survenue, veuillez nous en excuser' })
  }
}

exports.updateUserInfo = async (req, res) => {
  try {
    const userId = req.params.id
    const user = await models.User.findByPk(userId)
    let userObject
    if (user.profilePic && req.file) {
      const filename = user.profilePic.split('/images/')[1]
      fs.unlink(`images/${filename}`, async () => {
        try {
          userObject = {
            ...req.body,
            profilePic: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
          }
          await models.User.update(userObject, {
            where: { id: userId }
          })
          res.status(200).json({ message: 'User updated successfully' })
        } catch (error) {
          res.status(500).json({ error: 'Oups ! Une erreur est survenue, veuillez nous en excuser' })
        }
      })
    }
    userObject = {
      ...req.body
    }
    await models.User.update(userObject, {
      where: { id: userId }
    })
    res.status(200).json({ message: 'User updated successfully' })
  } catch (error) {
    res.status(500).json({ error: 'Oups ! Une erreur est survenue, veuillez nous en excuser' })
  }
}

exports.getAllUsers = async (req, res) => {
  try {
    const users = await models.User.findAll()
    res.status(200).json(users)
  } catch (error) {
    res.status(500).json({ error: 'Oups ! Une erreur est survenue, veuillez nous en excuser' })
  }
}

exports.getUser = async (req, res) => {
  try {
    const userId = req.params.id || req.body.id
    const user = await models.User.findByPk(userId)
    if (user === null) {
      res.status(404).json({ error: 'Utilisateur non trouvé' })
    } else {
      res.status(200).json(user)
    }
  } catch (error) {
    res.status(404).json({ error: 'Utilisateur non trouvé' })
  }
}

exports.deleteUser = async (req, res) => {
  try {
    const userId = req.params.id
    const user = await models.User.findByPk(userId)
    if (user.profilePic) {
      const filename = user.profilePic.split('/images/')[1]
      fs.unlink(`images/${filename}`, async () => {
        try {
          await models.User.destroy({
            where: { id: userId }
          })
          res.status(200).json({ message: 'User deleted successfully' })
        } catch (error) {
          res.status(400).json({ error: 'Requête incorrecte' })
        }
      })
    }
    await models.User.destroy({
      where: { id: userId }
    })
    res.status(200).json({ message: 'User deleted successfully' })
  } catch (error) {
    res.status(400).json({ error: 'Requête incorrecte' })
  }
}
