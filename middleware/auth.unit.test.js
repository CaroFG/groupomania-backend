const auth = require('./auth')
const httpMocks = require('node-mocks-http')
require('dotenv').config()
let req, res, next

beforeEach(() => {
  req = httpMocks.createRequest()
  res = httpMocks.createResponse()
  next = jest.fn()
  req.session = { destroy: jest.fn() }
})

test('should throw an error if there is no cookie', () => {
  req.headers.cookie = null
  try {
    auth(req, res, next)
    expect(res.statusCode).toBe(401)
  } catch (err) {
    expect(true).toBe('false')
  }
})
test('should send 401 status code and destroy session if cookieId and req.sessionID do not match', () => {
  req.sessionID = 'mySessionID'
  req.headers.cookie = process.env.COOKIE_ST + 'notSessionID' + '.somestring'
  req.session = { userId: 5, destroy: jest.fn().mockReturnValue(res.statusCode = 401) }
  try {
    auth(req, res, next)
    expect(req.session.destroy).toBeCalled()
    expect(res.statusCode).toBe(401)
  } catch (err) {
    expect(true).toBe('false')
  }
})

test('should call next if cookieId and req.sessionID match and req.session.userId exist', () => {
  req.sessionID = 'mySessionID'
  req.headers.cookie = process.env.COOKIE_ST + req.sessionID + '.somestring'
  req.session = { userId: 5, destroy: jest.fn() }

  try {
    auth(req, res, next)
    expect(next).toBeCalled()
  } catch (err) {
    expect(true).toBe('false')
  }
})
