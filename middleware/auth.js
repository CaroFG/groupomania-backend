require('dotenv').config()

module.exports = (req, res, next) => {
  try {
    const cookie = req.headers.cookie.split('.')[0]
    const cookieId = cookie.replace(process.env.COOKIE_ST, '')
    if ((cookieId === req.sessionID) && req.session.userId) {
      next()
    } else {
      req.session.destroy(function (err) {
        if (err) {
          console.error(err)
        }
        res.status(401).json(new Error('Authentication required'))
      })
    }
  } catch (error) {
    res.status(401).json({ error })
  }
}
