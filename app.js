const express = require('express')
const session = require('express-session')
const db = require('./models/index')
const SequelizeStore = require('connect-session-sequelize')(session.Store)
const app = express()
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
const path = require('path')

const userRoutes = require('./routes/user.routes')
const postRoutes = require('./routes/post.routes')
const commentRoutes = require('./routes/comment.routes')
dotenv.config()

const myStore = new SequelizeStore({
  db: db.sequelize
})

const sess = {
  name: process.env.SESS_NAME,
  resave: false,
  saveUninitialized: false,
  secret: process.env.SESSION_SECRET,
  store: myStore,
  cookie: {
    maxAge: (1000 * 60 * 60 * 2),
    sameSite: 'none'
  }
}

myStore.sync()

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())
app.use('/images', express.static(path.join(__dirname, 'images')))

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
  res.setHeader('Access-Control-Allow-Credentials', true)
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
  next()
})

app.use(session(sess))

app.use('/api/users', userRoutes)
app.use('/api/posts', postRoutes)
app.use('/api/posts', commentRoutes)

module.exports = { app }
