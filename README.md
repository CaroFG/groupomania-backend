# Groupomania backend

7th project of the OpenClassrrooms' path: Web Developer

> Build a Full-Stack Solution
Analyze a client’s needs to define the scope and features of a new application. With the help of a front-end framework, build a full-stack solution, including a SQL database.

🔧 Skills acquired in this project

- Build a full-stack solution
- Store data securely using SQL
- Send personalized content to a client
- Manage a user session

## start

```bash
npm install
npm start
```